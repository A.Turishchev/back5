<!DOCTYPE html>

<html lang="ru">
    <head>
        <meta charset="UTF-8">
        <title>
            Back 3
        </title>

        <link rel="stylesheet" href="style.css">

    </head>

    <body>
        <header>
          <div class="block">
            <img id="img" src="logo.jpg" alt="logo">
          </div>

          <div id="name">Back 3</div>
        </header>

        <nav>
            <a id="r" class="hr" href="#reffers">Ссылки</a>

            <a class="hr" href="#table">Таблица</a>

            <a id="l" class="hr" href="#form">Форма</a>
        </nav>

      <div class="block">
      <div id="reffers">

        <p><a id="top"></a></p>

      <div id="form">

        <h2>Форма</h2>
   <?php
                      if (!empty($messages)) {
                        print('<div id="messages">');
                        foreach ($messages as $message) {
                          print($message);
                          }
                        print('</div>');
                      }
                    ?>

    <form action="example.com" method="POST">

      <label> Имя<br/>
        <input name="field-name" <?php if ($errors['field-name']) {print "class='error'" ;} ?> value="<?php print $values['field-name']; ?>" />
      </label><br/>

      <label> E-mail:<br/>
        <input name="field-email" <?php if ($errors['field-email']) {print "class='error'" ;} ?> value="<?php print $values['field-email']; ?>" />
      </label><br/>

      <label> Дата Рождения:<br/>
        <input name="field-date" <?php if ($errors['field-date']) {print "class='error'" ;} ?> value="<?php print $values['field-date']; ?>" />
      </label><br/>

      Пол:<br/>
     <label><input type="radio" name="radio1" 
                    <?php if ($errors['radio1']) {print 'class="error"' ;} if($values['radio1']=="М"){print "checked='checked'";}?> value="М" />
            М</label>
    <label><input type="radio" name="radio2" <?php if ($errors['radio2']) {print 'class="error"' ;} if($values['radio2']=="Ж"){print "checked='checked'";}?> value="Ж" />
            Ж</label><br />


      Кол-во конечностей:<br/>
      <label><input type="radio" name="radio-group-2" <?php if ($errors['radio-group-2']) {print 'class="error"' ;} if($values['radio-group-2']=="2"){print "checked='checked'";}?> value="4" />
                2</label>
              <label><input type="radio" name="radio-group-2" <?php if ($errors['radio-group-2']) {print 'class="error"' ;} if($values['radio-group-2']=="4"){print "checked='checked'";}?> value="4" />
                4</label>
              <label><input type="radio" name="radio-group-2" <?php if ($errors['radio-group-2']) {print 'class="error"' ;} if($values['radio-group-2']=="8"){print "checked='checked'";}?> value="8" />
                8</label>
                <label><input type="radio" name="radio-group-2" <?php if ($errors['radio-group-2']) {print 'class="error"' ;} if($values['radio-group-2']=="12"){print "checked='checked'";}?> value="12" />
                12</label><br />

        <label>
            Сверхспособности:
            <br/>
            <select name="field-name-4[]" multiple="multiple">
                  <option value="Бессмертие" <?php if($values['field-name-4']=="Бессмертие"){print "selected='selected'";}?>>Бессмертие</option>
                  <option value="Прохождение сквозь стены" <?php if($values['field-name-4']=="Прохождение сквозь стены"){print "selected='selected'";}?>>Прохождение сквозь стены</option>
                  <option value="Левитация" <?php if($values['field-name-4']=="Левитация"){print "selected='selected'";}?>>Левитация</option>
                </select>
              </label><br />


      <label>
        Биография:<br/>
        <textarea name="field-name2"><?php if ($errors['field-name-2']) {print 'class="error"' ;} ?>><?php print $values['field-name-2']; ?> </textarea>
      </label><br/>

      С контрактом ознакомлен<br/>
      <label><input type="checkbox" checked="checked" name="check-1" <?php  if($values['check-1']=="1"){print "checked='checked'";}?> value = "1"/>
        Согласен со всем</label><br/>

      <input type="submit" value="Отправить"/>

              <?php
              if(!empty($_SESSION['login']))
              print('<input type="submit" name="exit" value="Выйти" />');
              ?>


    </form>

    <p><a id="bottom"></a></p>
    </div></div>

      <footer>
        <p id="last">(с) Александр Турищев 2020</p>
      </footer>
          </body>
</html>
